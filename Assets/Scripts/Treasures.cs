﻿using UnityEngine;
using System.Collections;

public class Treasures : MonoBehaviour
{
    public enum TreasuresType { Gold, Health, EndLevel }
    
    public TreasuresType treasureType;
    public int awardCount = 10;
    bool isNeedSway = true;

    Vector3 startPosition;

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            if (coll.gameObject.GetComponent<Main>().HumanForm)
            {
                if(treasureType == TreasuresType.Gold)
                {
                    Main.ReceiveAward(awardCount);
                }
                else if (treasureType == TreasuresType.Health)
                {
                    Main.Instance.ChangeHealth(1);
                }
                else if (treasureType == TreasuresType.EndLevel)
                {
                    UIScript.OnLevelComplete(true);
                }
                CollapseTreasure();
            }
        }
    }

    public void CollapseTreasure()
    {
        GameObject getParticle = Instantiate(Resources.Load("Prefabs/TreasureGet")) as GameObject;
        if (treasureType == TreasuresType.Health)
        {
            getParticle.GetComponent<ParticleSystem>().startColor = new Color(1f, 0f, 0f, 1f);
        }
        else if (treasureType == TreasuresType.Gold)
        {
            getParticle.GetComponent<ParticleSystem>().startColor = Color.yellow;
        }
        else if (treasureType == TreasuresType.EndLevel)
        {
            getParticle.GetComponent<ParticleSystem>().startColor = GetComponent<SpriteRenderer>().color;
        }
        getParticle.transform.position = transform.position;
        Destroy(getParticle, 2f);

        Destroy(gameObject);
    }

    void Awake()
    {
        startPosition = transform.position;
    }

    void Update()
    {
        if (isNeedSway)
        {
            float yPos = Mathf.Sin(Time.time) * 0.1f;
            transform.position = new Vector3(startPosition.x, startPosition.y + yPos, startPosition.z);
        }
    }
}
