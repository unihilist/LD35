﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour
{

    public float Limit = 20;
    public GameObject Patrticles;
    // Use this for initialization
    void Start()
    {

    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Player" && !coll.gameObject.GetComponent<Main>().HumanForm || coll.gameObject.tag != "Player")
        {
            if (coll.relativeVelocity.magnitude > Limit)
            {
                if (Patrticles != null)
                {
                    Vector3 particlesPosition = transform.position;
                    //  particlesPosition.z = -10f;

                    GameObject bloodObject = Instantiate(Patrticles, particlesPosition, transform.rotation) as GameObject;
                    // Destroy(bloodObject, 3f);
                }
                if (name.Contains("Wall"))
                {
                    Sounds.PlayWallBroke();
                }
                Destroy(gameObject);
                // Main.Instance.AddEnergy(1);
            }
            else
            {
                if (coll.gameObject.name.Contains("Balka"))
                    Sounds.PlayMetal();
                if (coll.gameObject.tag == "Player" && !coll.gameObject.GetComponent<Main>().HumanForm&& coll.relativeVelocity.magnitude > 3)
                    Sounds.CreateWallBit(coll.contacts[0].point);
            }
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
